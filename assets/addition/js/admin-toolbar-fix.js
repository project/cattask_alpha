/**
 * Dynamic fix the top of header region
 **/

(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    setInterval(function(){
      var header_height = $('body').css('paddingTop');
      var margin_left = $('body').css('marginLeft');
      var is_fixed_system_toolbar = $('body').hasClass('toolbar-fixed');
      var is_home_header = $('header.admin-toolbar-fix').hasClass('home-header');
      var is_fixed_header = $('header.admin-toolbar-fix').hasClass('header_fixed');
      if (is_home_header) {
        // Hacking for homepage header
        if (!is_fixed_system_toolbar && is_fixed_header) $('header.admin-toolbar-fix').css('top', 0);
        else $('header.admin-toolbar-fix').css('top', header_height);

        $('header.admin-toolbar-fix').css('marginLeft', margin_left);
      } else {
        // Hacjing for none-homepage header
        $('header.admin-toolbar-fix').css('top', 0);
        if (is_fixed_system_toolbar) $('header.admin-toolbar-fix.header_fixed').css('top', header_height);
      }
      $('header.admin-toolbar-fix').css('width', 'calc(100vw - ' + margin_left + ')');
    }, 100);
  });
})(jQuery, Drupal, drupalSettings);

