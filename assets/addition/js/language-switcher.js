/**
 * Dynamic fix the top of header region
 **/

(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    var language_name = $('.language-switcher .nav-item.is-active a').text();
    $('.language-switcher .dropdown-toggle').text(language_name);
  });
})(jQuery, Drupal, drupalSettings);

